# Leica Camera 5.0 for Mi 9T Pro/Redmi K20 Pro (raphael) AOSP

### Cloning :
- Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/ItzDFPlayer/vendor_xiaomi_miuicamera -b leica-5.0-raphael vendor/xiaomi/miuicamera
```

Make these changes in **sm8150-common**

**sm8150.mk**
```
# MiuiCamera
$(call inherit-product, vendor/xiaomi/miuicamera/config.mk)
```
## Credits

### Original mod - https://github.com/a406010503/Miui_Camera

## Support

### https://t.me/itzdfplayer_stash <br>

